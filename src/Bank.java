import java.util.Scanner;

public class Bank {

    String nameBank;
    float currencyUSD;
    float currencyEUR;
    float currencyZL;

    Bank[] banks = new Bank[3];

    public static void showBanks() {
        String[] namesBanks = {"ПриватБанк", "ОщадБанк", "УкрСибБанк"};

        for (int i = 0; i < namesBanks.length; i++) {
            System.out.println(namesBanks[i]);
        }
    }

    public void generateBanks() {
        String[] namesBanks = {"ПриватБанк", "ОщадБанк", "УкрСибБанк"};
        float[] currencysUSD = {27, 28.5f, 27.5f};
        float[] currencysEUR = {30.2f, 31.0f, 30.5f};

        for (int i = 0; i < namesBanks.length; i++) {
            Bank bank = new Bank();
            bank.nameBank = namesBanks[i];
            bank.currencyUSD = currencysUSD[i];
            bank.currencyEUR = currencysEUR[i];
        }
    }


    public void Convertation(String choisedBank, Bank bank) {
        if (choisedBank.equals("приватбанк") || choisedBank.equals("1")) {
            Scanner input = new Scanner(System.in);
            String choisedCurrency;
            int choisedCount;

            System.out.println("Введите валюту на которую хотите поменять гривну: ");
            choisedCurrency = input.nextLine();

            if (choisedCurrency.equalsIgnoreCase("usd")) {
                System.out.println("Введите кол-во валюты которую хотите сконвертировать:");
                choisedCount = input.nextInt();


                float result = choisedCount * bank.currencyUSD;

                System.out.println("Результат: " + result);
            }
        }
    }
}
